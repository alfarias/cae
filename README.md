# Sistema para a classificação de arritmias através de uma Rede Neural Convolucional Auto-associativa

Código fonte para a implementação apresentada no artigo [A Competitive Structure of Convolutional Autoencoder Networks for Electrocardiogram Signals Classification](https://sol.sbc.org.br/index.php/eniac/article/view/4446)
no XV Encontro Nacional de Inteligência Artificial e Computacional (ENIAC) em 2018.




